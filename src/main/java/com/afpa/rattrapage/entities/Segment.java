package com.afpa.rattrapage.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Segment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	
//	private Point pointA;
//	
//	private Point pointB;
	
//	@OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
	
	@ManyToMany(cascade = { CascadeType.ALL} )
	@JoinTable(
			name = "segments_points",
	joinColumns = { @JoinColumn(name = "id_point") },
	inverseJoinColumns = { @JoinColumn(name = "id_segment") }
	)
	private List<Point> listPoints;
	
	@Column(name = "t_longueur")
	private double longueur;
	
//	@Column(name = "t_nom")
//	private String nom;
//	
//	public Segment(Point a, Point b) {
//	}
//	
//	//longueur du segment
//	public double longueur() {
//		return Math.sqrt(Math.pow(pointB.getAbs(), pointA.getAbs(),2)-Math.pow(PointB.getOrd(), PointA.getOrd(),2));
//	}

}

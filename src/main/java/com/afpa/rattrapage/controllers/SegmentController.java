package com.afpa.rattrapage.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.rattrapage.dto.PointDto;
import com.afpa.rattrapage.dto.SegmentDto;
import com.afpa.rattrapage.service.ISegmentService;

@RestController
@RequestMapping("/segments")
public class SegmentController {
	
	@Autowired
	ISegmentService segmentServ;
	
	@GetMapping("/{id}")
	public SegmentDto segmentById(@PathVariable String id) {
		SegmentDto sDto = new SegmentDto();
		sDto.setId(Integer.parseInt(id));
		return this.segmentServ.segmentById(sDto);
	}
	
	@GetMapping
	public List<SegmentDto> listSegment() {
		return this.segmentServ.listSegment();
	}
	
	@PostMapping
	public void addSegment(@RequestBody SegmentDto segmentDto) {
		this.segmentServ.addSegment(segmentDto.getListPoints().get(0).getId(), segmentDto.getListPoints().get(1).getId());
//		SegmentDto sdto = this.segmentServ.addSegment(segmentDto);
//		return sdto;
	}
	
	@PutMapping
	public void putSegment(@RequestBody SegmentDto segmentDto) {
		this.segmentServ.putSegment(segmentDto);
	}
	
	@DeleteMapping("/{id}")
	public void deleteSegment(@PathVariable String id) {
		this.segmentServ.deleteSegment(Integer.parseInt(id));
	}
	
	@PostMapping("/list/{idS}/{idP}")
	public void addPointInSegment(@PathVariable String idS, @PathVariable String idP) {
		this.segmentServ.addPointInSegment(Integer.parseInt(idS), Integer.parseInt(idP));
	}
	
	@DeleteMapping("/list/{idS}/{idP}")
	public void deletePointInSegment(@PathVariable String idS, @PathVariable String idP) {
		this.segmentServ.deletePointInSegment(Integer.parseInt(idS), Integer.parseInt(idP));
	}
	
	@GetMapping("/list/{idS}")
	public List<PointDto> listPointInSegment(@PathVariable String id) {
		return this.segmentServ.listPointInSegment(Integer.parseInt(id));
	}

}

package com.afpa.rattrapage.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.rattrapage.entities.Point;

@Repository
public interface IDaoPoint extends CrudRepository<Point, Integer> {

	Optional<Point> findById(int id);
	
	Optional<Point> findByNomAndAbsAndOrd(String nom, double abs, double ord);
	
}

package com.afpa.rattrapage.service;

import java.util.List;

import com.afpa.rattrapage.dto.PointDto;
import com.afpa.rattrapage.dto.SegmentDto;

public interface IPointService {
	
	PointDto pointById(int id);
	
	List<PointDto> listPoint();
	
	PointDto addPoint(PointDto pointDto);
	
	void putPoint(PointDto pointDto);
	
	PointDto deletePoint(int parseInt);
	
	void addSegmentInPoint(int idS, int idP);
	
	void deleteSegmentInPoint(int idP, int idS);
	
	List<SegmentDto> listSegmentInPoint(PointDto pointDto);

}

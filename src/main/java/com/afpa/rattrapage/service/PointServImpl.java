package com.afpa.rattrapage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.rattrapage.dao.IDaoPoint;
import com.afpa.rattrapage.dao.IDaoSegment;
import com.afpa.rattrapage.dto.PointDto;
import com.afpa.rattrapage.dto.SegmentDto;
import com.afpa.rattrapage.entities.Point;
import com.afpa.rattrapage.entities.Segment;

@Service
public class PointServImpl implements IPointService {

	@Autowired
	IDaoPoint pointDao;
	
	@Autowired
	IDaoSegment segmentDao;
	
	@Autowired
	ModelMapper model;
	
	@Override
	public PointDto pointById(int id) {
		Point p = pointDao.findById(id).get();
		PointDto pdto = new PointDto();
		pdto.setId(p.getId());
		pdto.setAbs(p.getAbs());
		pdto.setOrd(p.getOrd());
		pdto.setNom(p.getNom());
		return pdto;
	}

	@Override
	public List<PointDto> listPoint() {
		List<PointDto> points = ((List<Point>)pointDao.findAll())
				.stream()
				.map(e-> {
					PointDto pdto = new PointDto();
					pdto.setId(e.getId());
					pdto.setAbs(e.getAbs());
					pdto.setOrd(e.getOrd());
					pdto.setNom(e.getNom());
					return pdto;
				})
				.collect(Collectors.toList());
		return points;
	}

	@Override
	public PointDto addPoint(PointDto pointDto) {
//		List<PointDto> pointDtos = new ArrayList<PointDto>();
//		for (PointDto pointDto2 : pointDtos) {
//
//		}
		if (pointDto.getNom().length() > 2 || pointDto.getNom().length() >= 0) {
			
		}
		
		return model.map(pointDao.save(model.map(pointDto, Point.class)), PointDto.class );
	}

	@Override
	public void putPoint(PointDto pointDto) {
		Point p = this.pointDao.findById(pointDto.getId()).get();
		p.setAbs(pointDto.getAbs());
		p.setOrd(pointDto.getOrd());
		p.setNom(pointDto.getNom());
		pointDao.save(p);
	}

	@Override
	public PointDto deletePoint(int parseInt) {
		pointDao.deleteById(parseInt);
		return null;

	}

	@Override
	public void addSegmentInPoint(int idP, int idS) {
		Point p = this.pointDao.findById(idP).get();	
		Segment s = this.segmentDao.findById(idS).get();
		List<Point> listPointInSegment = s.getListPoints();
		listPointInSegment.add(p);
		this.segmentDao.save(s);
	}

	@Override
	public void deleteSegmentInPoint(int idS, int idP) {
		Point p = this.pointDao.findById(idP).get();
		Segment s = this.segmentDao.findById(idP).get();
		List<Point> listPointInSegment = s.getListPoints();
		listPointInSegment.remove(p);
		this.segmentDao.save(s);
		
	}

	@Override
	public List<SegmentDto> listSegmentInPoint(PointDto pointDto) {
		Point p = this.pointDao.findById(pointDto.getId()).get();
		List<SegmentDto> segments = pointDto.getListSegments()
				.stream()
				.map(e -> SegmentDto.builder()
						.id(e.getId())
						.build())
				.collect(Collectors.toList());
		return segments;
	}

}

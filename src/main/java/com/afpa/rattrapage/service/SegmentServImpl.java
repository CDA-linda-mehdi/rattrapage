package com.afpa.rattrapage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.rattrapage.dao.IDaoPoint;
import com.afpa.rattrapage.dao.IDaoSegment;
import com.afpa.rattrapage.dto.PointDto;
import com.afpa.rattrapage.dto.SegmentDto;
import com.afpa.rattrapage.entities.Point;
import com.afpa.rattrapage.entities.Segment;

@Service
public class SegmentServImpl implements ISegmentService {

	@Autowired
	IDaoSegment segmentDao;

	@Autowired
	IDaoPoint pointDao;

	@Autowired
	ModelMapper model;

	@Override
	public SegmentDto segmentById(SegmentDto segmentDto) {
		Segment seg = this.segmentDao.findById(segmentDto.getId()).get();
		return SegmentDto.builder()
				.id(seg.getId())
				.listPoints(seg.getListPoints().stream().map(e -> PointDto.builder()
						.id(e.getId())
						.abs(e.getAbs())
						.ord(e.getOrd())
						.nom(e.getNom())
						.build())
				.collect(Collectors.toList()))
				
				.longueur(seg.getLongueur())
				.build();
//		Segment s = segmentDao.findById(id).get();
//		SegmentDto sdto = new SegmentDto();
//		sdto.setId(s.getId());
//		sdto.setLongueur(s.getLongueur());
//		sdto.setListPoints(s.getListPoints());
//		return sdto;
	}

	@Override
	public List<SegmentDto> listSegment() {
		List<Segment> listSeg = (List<Segment>) this.segmentDao.findAll();
		List<SegmentDto> listSegDto = listSeg.stream().map( e-> SegmentDto.builder()
				.id(e.getId())
				.listPoints(e.getListPoints().stream().map(p -> PointDto.builder()
						.id(p.getId())
						.abs(p.getAbs())
						.ord(p.getOrd())
						.nom(p.getNom())
						.build())
					.collect(Collectors.toList()))
						
				.longueur(e.getLongueur())
				.build())
				.collect(Collectors.toList());
		return listSegDto;
//		List<SegmentDto> segments = ((List<Segment>) segmentDao.findAll()).stream().map(e -> {
//			SegmentDto sdto = new SegmentDto();
//			sdto.setId(e.getId());
//			sdto.setLongueur(e.getLongueur());
//			sdto.setListPoints(e.getListPoints());
//			return sdto;
//		}).collect(Collectors.toList());
//		return segments;
	}

	@Override
	public String addSegment(int idPa, int idPb) {
		Segment sg = new Segment();
		List<Point> points = new ArrayList<Point>();
		System.out.println(idPa + " " + idPb);
		Point a = pointDao.findById(idPa).get();
		Point b = pointDao.findById(idPb).get();
		points.add(a);
		points.add(b);
		sg.setLongueur(this.longueur(a, b));
		sg.setListPoints(points);
		this.segmentDao.save(sg);
		return "ok";
//		return model.map(segmentDao.save(model.map(segmentDto, Segment.class)), SegmentDto.class);
	}

	@Override
	public void putSegment(SegmentDto segmentDto) {
		Segment s = this.segmentDao.findById(segmentDto.getId()).get();
		//s.setNom(segmentDto.getNom());
		s.setListPoints(segmentDto.getListPoints().stream().map(e -> Point.builder()
				.id(e.getId())
				.abs(e.getAbs())
				.ord(e.getOrd())
				.build())
				.collect(Collectors.toList()));
		segmentDao.save(s);
	}

	@Override
	public void deleteSegment(int parseInt) {
		segmentDao.deleteById(parseInt);
	}

	@Override
	public void addPointInSegment(int idSegment, int idPoint) {
		Point p = this.pointDao.findById(idPoint).get();
		Segment s = this.segmentDao.findById(idSegment).get();
		s.getListPoints().add(p);
		segmentDao.save(s);
	}

	@Override
	public void deletePointInSegment(int idSegment, int idPoint) {
		Point p = this.pointDao.findById(idPoint).get();
		Segment s = this.segmentDao.findById(idSegment).get();
		List<Point> points = s.getListPoints();
		points.remove(p);
		s.setListPoints(points);
		this.segmentDao.save(s);
	}

	@Override
	public List<PointDto> listPointInSegment(int parseInt) {
		List<Point> points = segmentDao.listPointById(parseInt);
		return null;
	}


	// méthode à finir
	@Override
	public double longueur(int idS, Point a, Point b) {
		double xa = a.getAbs();
		double ya = a.getOrd();
		double xb = b.getAbs();
		double yb = b.getOrd();
		return Math.sqrt((yb - ya) * (yb - ya) + (xb - xa) * (xb - xa));
		
	}
	
	public double longueur(Point a, Point b) {
		double xa = a.getAbs();
		double ya = a.getOrd();
		double xb = b.getAbs();
		double yb = b.getOrd();
		return Math.sqrt((yb - ya) * (yb - ya) + (xb - xa) * (xb - xa));
		
	}
//		List<Point> ls = new ArrayList<Point>();
//		Point a1 = new Point();
//		Point b1 = new Point();
//		ls.add(a1);
//		ls.add(b1);
//		Segment s = this.segmentDao.findById(idS).get();
//		double abs = s.getListPoints().get(idS).getAbs();
//		double ord = s.getListPoints().get(idS).getOrd();
//		s.getListPoints().add(a);
//		s.getListPoints().add(b);
//
//		return Math.sqrt(abs - ord);
//	}
}

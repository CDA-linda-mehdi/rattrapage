

export class Point {
    id: number;
    nom: string;
    abs: number;
    ord: number;

    constructor(id?: number, nom?: string, abs?: number, ord?: number) {
        this.id = id;
        this.nom = nom;
        this.abs = abs;
        this.ord = ord;
    }
}
import { Point } from './point';

export class Segment {
    id: number;
    nom: string;
    listPoints: Point[];
    longueur: number;

    constructor(nom?: string, points?: Point[], longueur?: number, id?: number) {
        this.id = id;
        this.nom = nom;
        this.listPoints = points;
        this.longueur = longueur;
    }    
}
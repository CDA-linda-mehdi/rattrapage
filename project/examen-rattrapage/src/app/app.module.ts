import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddPointComponent } from './module/point/add-point/add-point.component';
import { ListPointComponent } from './module/point/list-point/list-point.component';
import { PutPointComponent } from './module/point/put-point/put-point.component';
import { DetailPointComponent } from './module/point/detail-point/detail-point.component';
import { DetailSegmentComponent } from './module/segment/detail-segment/detail-segment.component';
import { PutSegmentComponent } from './module/segment/put-segment/put-segment.component';
import { ListSegmentComponent } from './module/segment/list-segment/list-segment.component';
import { AddSegmentComponent } from './module/segment/add-segment/add-segment.component';
import { HomePointComponent } from './module/point/home-point/home-point.component';
import { HomeSegmentComponent } from './module/segment/home-segment/home-segment.component';
import { HttpClientModule } from '@angular/common/http';
import { AccueilComponent } from './module/accueil/accueil/accueil.component';

@NgModule({
  declarations: [
    AppComponent,
    AddPointComponent,
    ListPointComponent,
    PutPointComponent,
    DetailPointComponent,
    DetailSegmentComponent,
    PutSegmentComponent,
    ListSegmentComponent,
    AddSegmentComponent,
    HomePointComponent,
    HomeSegmentComponent,
    AccueilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

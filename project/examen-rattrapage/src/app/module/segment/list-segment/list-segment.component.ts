import { Component, OnInit } from '@angular/core';
import { Segment } from 'src/app/model/segment';
import { SegmentService } from 'src/app/service/segmentService';
import { Router } from '@angular/router';
import { Point } from 'src/app/model/point';

@Component({
  selector: 'app-list-segment',
  templateUrl: './list-segment.component.html',
  styleUrls: ['./list-segment.component.css']
})
export class ListSegmentComponent implements OnInit {

  segments: Segment[];
  points1: Point[];
  constructor(private segmentService: SegmentService, private router: Router) { }

  ngOnInit() {
    this.segmentService.getAll().subscribe(
      res => {
        this.segments = res;
        console.log(this.segments);
      });
  }

  affiche(id: number) {
    console.log(id);
    this.router.navigateByUrl('segments/' + id);
  }
/* 
  redirectAddSegment() {
    this.router.navigateByUrl('/segments');
  } */

  retour(id: number) {
    this.router.navigateByUrl('/segments')
  }

  suppr(id: number) {
    if (confirm('Voulez-vous vraiment supprmer ce segment ?')) {
      this.segmentService.delete(id).subscribe(
        res => {
          this.segmentService.getAll().subscribe(
            donnees => {
              this.segments = donnees;
            }
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }

}

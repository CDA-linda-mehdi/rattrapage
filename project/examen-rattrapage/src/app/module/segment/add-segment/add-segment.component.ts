import { Component, OnInit } from '@angular/core';
import { Segment } from 'src/app/model/segment';
import { SegmentService } from 'src/app/service/segmentService';
import { Router } from '@angular/router';
import { Point } from 'src/app/model/point';
import { PointService } from 'src/app/service/pointService';

@Component({
  selector: 'app-add-segment',
  templateUrl: './add-segment.component.html',
  styleUrls: ['./add-segment.component.css']
})
export class AddSegmentComponent implements OnInit {

  segment: Segment;
  segment1: Segment;
  points: Point[];
  num1: number;
  num2: number;
  
  Points1 : Point;
  Points2 : Point;
  listepoints: any;
  
  constructor(private segmentService: SegmentService, private pointService: PointService, private router: Router) { }


  ngOnInit() {
    this.Points1 = new Point();
    this.Points2 = new Point();
    this.segment = new Segment();
    this.pointService.getAll().subscribe(res => {
      this.pointService.getAll().subscribe(
        donnees => {
          this.points = donnees;
        });
    });
  }

  addSegment() {
    
    this.Points1.id = this.num1;
    this.Points2.id = this.num2;
    this.listepoints = [this.Points1 , this.Points2];
    this.segment1 = new Segment(this.segment.nom,this.listepoints)
    console.log(this.segment1);
    this.segmentService.add(this.segment1).subscribe(
      res => {
       
        this.router.navigateByUrl('segments/list');
      },
      error => {
        console.log(error);
      }
    );
  }

  clear() {
    this.segment = new Segment();
  }

}

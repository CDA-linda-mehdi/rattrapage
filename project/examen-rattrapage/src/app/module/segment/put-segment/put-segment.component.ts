import { Component, OnInit } from '@angular/core';
import { Segment } from 'src/app/model/segment';
import { ActivatedRoute, Router } from '@angular/router';
import { SegmentService } from 'src/app/service/segmentService';
import { Point } from 'src/app/model/point';
import { PointService } from 'src/app/service/pointService';

@Component({
  selector: 'app-put-segment',
  templateUrl: './put-segment.component.html',
  styleUrls: ['./put-segment.component.css']
})
export class PutSegmentComponent implements OnInit {

 /*  segment: Segment;
  points: Point[]; */

  segment: Segment;
  segment1: Segment;
  points: Point[];
  num1: number;
  num2: number;
  
  Points1 : Point;
  Points2 : Point;
  listepoints: any;
  constructor(private segmentService: SegmentService, private pointService: PointService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  /*   this.segment = new Segment();
    this.segment.point1 = new Point();
    this.segment.point2 = new Point();
    this.route.paramMap.subscribe(res => {
      let id;
      id = res.get('id');
      this.segmentService.getById(id).subscribe(donnees => {
        console.log(donnees);
        this.segment = donnees;
      });
    }); */
    this.Points1 = new Point();
    this.Points2 = new Point();
    this.segment = new Segment();
    this.route.paramMap.subscribe(res => {
      let id;
      id = res.get('id');
      this.segmentService.getById(id).subscribe(donnees => {
        console.log(donnees);
        this.segment = donnees;
      })
    })
    this.pointService.getAll().subscribe(res => {
      this.pointService.getAll().subscribe(
        donnees => {
          this.points = donnees;
        });
    });
/*     
    this.pointService.getAll().subscribe(res => {
      this.pointService.getAll().subscribe(
        donnees => {
          this.points = donnees;
        });
    }); */
  }

  putSegment() {
    this.Points1.id = this.num1;
    this.Points2.id = this.num2;
    this.listepoints = [this.Points1 , this.Points2];
    this.segment1 = new Segment("",this.listepoints,0,this.segment.id);
    console.log(this.segment1);
    this.segmentService.put(this.segment1).subscribe(
      res => {
        this.router.navigateByUrl('segments/' + this.segment.id);
      },
      error => {
        console.log(error);
      }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { Segment } from 'src/app/model/segment';
import { SegmentService } from 'src/app/service/segmentService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail-segment',
  templateUrl: './detail-segment.component.html',
  styleUrls: ['./detail-segment.component.css']
})
export class DetailSegmentComponent implements OnInit {

  segment: Segment;
  constructor(private segmentService: SegmentService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.segment = new Segment();
    this.route.paramMap.subscribe(res => {
      let id;
      id = res.get('id');
      this.segmentService.getById(id).subscribe(donnees => {
        console.log(donnees);
        this.segment = donnees;
      });
    });
  }

  suppr(id: number) {
    if (confirm('Voules-vous vraiment supprimer ce point ?')) {
      this.segmentService.delete(id).subscribe(
      res => {
        console.log('Suppression bien effectuée');
        this.router.navigateByUrl('/points');
      },
      error => {
        console.log(error);
      }
      );
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { Point } from 'src/app/model/point';
import { PointService } from 'src/app/service/pointService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail-point',
  templateUrl: './detail-point.component.html',
  styleUrls: ['./detail-point.component.css']
})
export class DetailPointComponent implements OnInit {

  point: Point;
  constructor(private pointService: PointService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.point = new Point();
    this.route.paramMap.subscribe(res => {
      let id;
      id = res.get('id');
      this.pointService.getById(id).subscribe(donnees => {
        console.log(donnees);
        this.point = donnees;
      });
    });
  }

  suppr(id: number) {
    if (confirm('Voules-vous vraiment supprimer ce point ?')) {
      this.pointService.delete(id).subscribe(
      res => {
        console.log('Suppression bien effectuée');
        this.router.navigateByUrl('points/list');
      },
      error => {
        console.log(error);
      }
      );
    }
  }

}

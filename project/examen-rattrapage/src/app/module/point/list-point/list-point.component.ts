import { Component, OnInit } from '@angular/core';
import { Point } from 'src/app/model/point';
import { Router } from '@angular/router';
import { PointService } from 'src/app/service/pointService';

@Component({
  selector: 'app-list-point',
  templateUrl: './list-point.component.html',
  styleUrls: ['./list-point.component.css']
})
export class ListPointComponent implements OnInit {

  points: Point[];
  constructor(private pointService: PointService, private router: Router) { }

  ngOnInit() {
    this.pointService.getAll().subscribe(
      res => {
        this.points = res;
      });
  }

  affiche(id: number) {
    console.log(id);
    this.router.navigateByUrl('points/' + id);
  }
/* 
  redirectAddPoint() {
    this.router.navigateByUrl('/points');
  } */

  retour(id: number) {
    this.router.navigateByUrl('/points')
  }

  suppr(id: number) {
    if (confirm('Voulez-vous vraiment supprmer ce point ?')) {
      this.pointService.delete(id).subscribe(
        res => {
          this.pointService.getAll().subscribe(
            donnees => {
              this.points = donnees;
            }
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }

}

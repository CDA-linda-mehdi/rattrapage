import { Component, OnInit } from '@angular/core';
import { Point } from 'src/app/model/point';
import { PointService } from 'src/app/service/pointService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-point',
  templateUrl: './add-point.component.html',
  styleUrls: ['./add-point.component.css']
})
export class AddPointComponent implements OnInit {

  point: Point;
  constructor(private pointService: PointService, private router: Router) { }


  ngOnInit() {
    this.point = new Point();
  }

  addPoint() {
    this.pointService.add(this.point).subscribe(
      res => {
        this.router.navigateByUrl('points/list');
      },
      error => {
        console.log(error);
      }
    );
  }

  clear() {
    this.point = new Point();
  }

}

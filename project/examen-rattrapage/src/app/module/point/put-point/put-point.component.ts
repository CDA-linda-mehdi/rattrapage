import { Component, OnInit, PipeTransform } from '@angular/core';
import { Point } from 'src/app/model/point';
import { PointService } from 'src/app/service/pointService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-put-point',
  templateUrl: './put-point.component.html',
  styleUrls: ['./put-point.component.css']
})
export class PutPointComponent implements OnInit {
  
  point: Point;
  constructor(private pointService: PointService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.point = new Point();
    this.route.paramMap.subscribe(res => {
      let id;
      id = res.get('id');
      this.pointService.getById(id).subscribe(donnees => {
        console.log(donnees);
        this.point = donnees;
      });
    });
  }

  putPoint() {
    this.pointService.put(this.point).subscribe(
      res => {
        this.router.navigateByUrl('/points/' + this.point.id);
      },
      error => {
        console.log(error);
      }
    );
  }

}

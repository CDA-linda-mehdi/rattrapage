import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Segment } from '../model/segment';

@Injectable({
    providedIn: 'root'
})

export class SegmentService {
    
    monUrl = 'http://localhost:8080/segments';

    segment: Segment;

    constructor(private http: HttpClient) { }

    getById(id: number): Observable<any> {
        return this.http.get(this.monUrl + '/' + id)
    }

    getAll(): Observable<any> {
        return this.http.get(this.monUrl);
    }

    add(segment: Segment): Observable<any> {
        return this.http.post(this.monUrl, segment);
    }

    put(segment: Segment) {
        return this.http.put(this.monUrl, segment);
    }

    delete(id: number) {
        return this.http.delete(this.monUrl + '/' + id);
    }
}
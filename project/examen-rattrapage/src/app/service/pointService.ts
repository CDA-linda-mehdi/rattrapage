import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Point } from '../model/point';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class PointService {
    
    monUrl = 'http://localhost:8080/points';

    point: Point;

    constructor(private http: HttpClient) { }

    getById(id: number): Observable<any> {
        return this.http.get(this.monUrl + '/' + id)
    }

    getAll(): Observable<any> {
        return this.http.get(this.monUrl);
    }

    add(point: Point): Observable<any> {
        return this.http.post(this.monUrl, point);
    }

    put(point: Point) {
        return this.http.put(this.monUrl, point);
    }

    delete(id: number) {
        return this.http.delete(this.monUrl + '/' + id);
    }
}
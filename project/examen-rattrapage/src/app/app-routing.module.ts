import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePointComponent } from './module/point/home-point/home-point.component';
import { AddPointComponent } from './module/point/add-point/add-point.component';
import { DetailPointComponent } from './module/point/detail-point/detail-point.component';
import { ListPointComponent } from './module/point/list-point/list-point.component';
import { PutPointComponent } from './module/point/put-point/put-point.component';
import { HomeSegmentComponent } from './module/segment/home-segment/home-segment.component';
import { ListSegmentComponent } from './module/segment/list-segment/list-segment.component';
import { AddSegmentComponent } from './module/segment/add-segment/add-segment.component';
import { PutSegmentComponent } from './module/segment/put-segment/put-segment.component';
import { DetailSegmentComponent } from './module/segment/detail-segment/detail-segment.component';
import { AccueilComponent } from './module/accueil/accueil/accueil.component';


const routes: Routes = [
  { path:'', component: AccueilComponent },

  
  
  {
    path:'points', component: HomePointComponent, children: [
      
        { path:'list', component: ListPointComponent },
        { path:'add', component: AddPointComponent },
        { path:'put/:id', component: PutPointComponent },
        { path:':id', component: DetailPointComponent} 
      
    ]
  },
  {
    path:'segments', component: HomeSegmentComponent, children: [
      
      { path:'list', component: ListSegmentComponent },
      { path:'add', component: AddSegmentComponent },
      { path:'put/:id', component: PutSegmentComponent },
      { path:':id', component: DetailSegmentComponent } 
    
  ]
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
